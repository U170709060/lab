class GCDLoop {

	public static void main(String[] args) {
		int number1 = Integer.parseInt(args[0]);
		int number2 = Integer.parseInt(args[1]);
		
		int divisor = gcd(number1>number2 ? number1 : number2,number1>number2 ? number2:number1);
		System.out.println("GCD is "+ divisor);
	}
	private static int gcd(int num,int num2) {
	int last=0;
	int reminder;
	do {
		reminder = num % num2;
		num = num2;
		num2 = reminder;
		last = num;
    }while (reminder!=0);

	return last;
    }

}


	s
